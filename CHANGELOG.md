## [6.7.1] - 2020-03-16
### Fixed
 - Error loading ima ads with npm solution
 - Minor fixes to prevent warning logs

## [6.7.0] - 2020-03-12
### Fixed
 - AdManager check to prevent errors when the player has no ads integrated

## [6.5.0] - 2019-11-27
### Added
 - First version
