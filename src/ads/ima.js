var youbora = require('youboralib')
var imaAdapter = require('youbora-adapter-ima')

var Ima = imaAdapter.extend({
  getPosition: function (e) {
    var ret = youbora.Constants.AdPosition.Preroll
    if (this.plugin.getAdapter().flags.isJoined) {
      if (this.plugin.getAdapter().getDuration() < this.plugin.getAdapter().getPlayhead() + 1) {
        ret = youbora.Constants.AdPosition.Postroll
      } else {
        ret = youbora.Constants.AdPosition.Midroll
      }
    }
    return ret
  },
  playingListener: function (e) {
    this.totalAds = e.getAd().getAdPodInfo().getTotalAds()
    this.plugin.fireInit()
    this.fireStart()
    this.fireResume()
    this.fireSeekEnd()
    this.fireBufferEnd()
    this.fireJoin({ adPlayhead: '0' })
  },
  getAudioEnabled: function () {
    return null
  }
})

module.exports = Ima
