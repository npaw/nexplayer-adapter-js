var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.NexPlayer = youbora.Adapter.extend({
  constructor: function (player, tag) {
    this.tag = tag
    youbora.adapters.NexPlayer.__super__.constructor.call(this, player)
  },

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.getCurrentTime()
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    return this.player.player_.videoElement_.playbackRate || 1
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    var ret = null
    if (this.player.player_ && this.player.player_.videoElement_) {
      ret = this.player.player_.videoElement_.webkitDroppedFrameCount
    }
    return ret
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.getDuration()
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var ret = null
    try {
      var trackInfo = this.player.getCurrentTrack()
      if (trackInfo) {
        ret = trackInfo.bitrate
      }
    } catch (err) {

    }
    return ret
  },

  /** Override to return rendition */
  getRendition: function () {
    var ret = null
    try {
      var trackInfo = this.player.getCurrentTrack()
      if (trackInfo) {
        ret = youbora.Util.buildRenditionString(trackInfo.width, trackInfo.height, trackInfo.bitrate)
      }
    } catch (err) {

    }
    return ret
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.player.isLive()
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.player_.URL
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'NexPlayer'
  },

  getLatency: function () {
    var ret = null
    try {
      ret = this.player.getCurrentLiveLatency()
      if (isNaN(ret) && !ret) {
        ret = null
      }
    } catch (err) {

    }
    return ret
  },

  registerListeners: function () {
    youbora.Util.logAllEvents(this.tag, [
      null, 'abort', 'canplay', 'canplaythrough', 'durationchange', 'emptied', 'encrypted',
      'interruptbegin', 'interruptend', 'loadeddata', 'loadedmetadata', 'loadstart',
      'mozaudioavailable', 'progress', 'ratechange', 'stalled', 'suspend', 'timeupdate',
      'volumechange', 'waiting', 'ended', 'error', 'pause', 'play', 'playing', 'seeked',
      'seeking', 'loadeddata'
    ])

    this.monitorPlayhead(true, false)

    this.references = {
      ended: this.endedListener.bind(this),
      error: this.errorListener.bind(this),
      pause: this.pauseListener.bind(this),
      play: this.playListener.bind(this),
      playing: this.playingListener.bind(this),
      seeked: this.seekedListener.bind(this),
      seeking: this.seekingListener.bind(this),
      loadstart: this.startAdsDetectionListener.bind(this),
      canplaythrough: this.startAdsDetectionListener.bind(this),
      timeupdate: this.timeupdateListener.bind(this)
    }

    // Register listeners
    for (var key in this.references) {
      this.tag.addEventListener(key, this.references[key])
    }

    setTimeout(function () { this.startAdsDetectionListener() }.bind(this), 100)
  },

  unregisterListeners: function () {
    if (this.monitor) this.monitor.stop()
    if (this.tag && this.references) {
      for (var key in this.references) {
        this.tag.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  /** Listener for 'loadeddata' event. */
  startAdsDetectionListener: function () {
    if (!this.plugin.getAdsAdapter() && typeof this.player.adManager === 'function') { // && typeof google !== 'undefined') {
      if (this.player.adManager().getAdObject()) {
        this.plugin.setAdsAdapter(new youbora.adapters.NexPlayer.Ima(this.player.adManager().getAdObject()))
      } else if (!this.flags.isJoined) {
        setTimeout(this.startAdsDetectionListener.bind(this), 400)
      }
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireStart()
    this.startAdsDetectionListener()
  },

  /** Listener for 'timeupdate' event. */
  timeupdateListener: function (e) {
    if (this.getPlayhead() > 0.1) {
      this.fireStart()
      this.fireJoin()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.startAdsDetectionListener()
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
    this.fireSeekEnd()
    this.fireBufferEnd()
    this.fireJoin()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    this.fireError()
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    if (this.flags.isBuffering) {
      this.fireBufferEnd()
    } else {
      this.fireSeekBegin()
    }
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    if (!this.player.player_.videoElement_.paused) {
      this.fireSeekEnd()
    }
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop()
  }
}, {
  Ima: require('./ads/ima')
}
)

module.exports = youbora.adapters.NexPlayer
